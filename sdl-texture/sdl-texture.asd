(asdf:defsystem :sdl-texture
  :serial t
  :pathname "src/lisp"
  :components ((:file "package")
               (:file "sdl-texture"))
  :depends-on (:asdf :sdl2 :sdl2-image))
