(in-package :sdl-texture)

(defun main-loop (event)
  (let ((event-quit))
    (loop
       :until event-quit
       :do
         (sdl2:next-event event :poll nil)
         (let* ((event-type (sdl2:get-event-type event)))
           (if (eq :QUIT event-type)
               (setf event-quit t))
         (sdl2:delay 10)))))

(defun main ()
  (sdl2:init :video)
  (let* ((window (sdl2:create-window :title "SDL Texture"
                                     :x sdl2-ffi:+sdl-windowpos-centered+
                                     :y sdl2-ffi:+sdl-windowpos-centered+
                                     :w 800
                                     :h 600
                                     :flags '(:opengl)))
         (renderer (sdl2:create-renderer window nil '(:ACCELERATED))))

    (let ((offscreen1 (sdl2:create-texture renderer :rgba8888 :target 800 600))
          (offscreen2 (sdl2:create-texture renderer :rgba8888 :target 800 600))
          (offscreen3 (sdl2:create-texture renderer :rgba8888 :target 800 600)))
      (sdl2:set-texture-blend-mode offscreen1 :none)
      (sdl2:set-texture-blend-mode offscreen2 :blend)
      (sdl2:set-texture-blend-mode offscreen3 :blend)

      (let* ((image (sdl2-image:load-image "resources/images/lisp.png"))
             (width (sdl2:surface-width image))
             (height (sdl2:surface-height image))
             (tex-image (sdl2:create-texture-from-surface renderer image)))

        (sdl2:set-render-target renderer offscreen1)
        (sdl2:set-render-draw-color renderer 0 0 0 #xFF)
        (sdl2:render-clear renderer)
        (sdl2:set-render-draw-color renderer 0 0 #xFF #xFF)
        (sdl2:render-fill-rect renderer (sdl2:make-rect 10 10 780 580))

        (sdl2:set-render-target renderer offscreen2)
        (sdl2:set-render-draw-color renderer 0 0 0 0)
        (sdl2:render-clear renderer)
        (sdl2:render-copy renderer tex-image
                          :source-rect (sdl2:make-rect 0 0 width height)
                          :dest-rect nil)
        (sdl2:set-render-target renderer offscreen3)
        (sdl2:set-render-draw-color renderer 0 0 0 0)
        (sdl2:render-clear renderer)
        (sdl2:set-render-draw-color renderer #xFF #xFF 0 200)
        (sdl2:render-fill-rect renderer (sdl2:make-rect 20 20 400 560))

        (sdl2:free-surface image)
        (sdl2:destroy-texture tex-image))

      (sdl2:set-render-target renderer nil)
      (sdl2:render-clear renderer)
      (sdl2:render-copy renderer offscreen1
                       :source-rect nil
                       :dest-rect nil)
      (sdl2:render-copy renderer offscreen2
                       :source-rect nil
                       :dest-rect nil)
      (sdl2:render-copy renderer offscreen3
                       :source-rect nil
                       :dest-rect nil)

      (sdl2:destroy-texture offscreen1)
      (sdl2:destroy-texture offscreen2)
      (sdl2:destroy-texture offscreen3))

    (sdl2:render-present renderer)
    (sdl2:destroy-renderer renderer)

    (let ((event nil))
      (unwind-protect (setf sdl2::*event-loop* t)
        (sdl2:in-main-thread
         (:background nil)
         (sdl2:with-sdl-event (event)
           (main-loop event)))
        (setf sdl2::*event-loop* nil)))
    (sdl2:destroy-window window))
  (sdl2:quit))
