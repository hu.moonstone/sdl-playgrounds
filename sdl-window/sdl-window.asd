(asdf:defsystem :sdl-window
  :serial t
  :pathname "src/lisp"
  :components ((:file "package")
               (:file "sdl-window"))
  :depends-on (:asdf :sdl2))
