(in-package :sdl-window)

(defun main-loop (event)
  (let ((event-quit))
    (loop
       :until event-quit
       :do
         (sdl2:next-event event :poll nil)
         (let* ((event-type (sdl2:get-event-type event)))
           (if (eq :QUIT event-type)
               (setf event-quit t))
         (sdl2:delay 10)))))

(defun main ()
  (sdl2:init :video)
  (let* ((window (sdl2:create-window :title "SDL Window"
                                     :x sdl2-ffi:+sdl-windowpos-centered+
                                     :y sdl2-ffi:+sdl-windowpos-centered+
                                     :w 800
                                     :h 600
                                     :flags '(:opengl)))
         (renderer (sdl2:create-renderer window nil '(:ACCELERATED))))
    (sdl2:render-clear renderer)
    (sdl2:set-render-draw-color renderer 0 0 #xFF #xFF)
    (sdl2:render-fill-rect renderer (sdl2:make-rect 10 10 780 580))
    (sdl2:render-present renderer)
    (sdl2:destroy-renderer renderer)

    (let ((event nil))
      (unwind-protect (setf sdl2::*event-loop* t)
        (sdl2:in-main-thread
         (:background nil)
         (sdl2:with-sdl-event (event)
           (main-loop event)))
        (setf sdl2::*event-loop* nil)))
    (sdl2:destroy-window window))
  (sdl2:quit))
