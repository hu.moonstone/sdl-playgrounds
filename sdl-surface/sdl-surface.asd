(asdf:defsystem :sdl-surface
  :serial t
  :pathname "src/lisp"
  :components ((:file "package")
               (:file "sdl-surface"))
  :depends-on (:asdf :sdl2 :sdl2-image))
