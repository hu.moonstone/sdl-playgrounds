(in-package :sdl-surface)

(defun main-loop (event)
  (let ((event-quit))
    (loop
       :until event-quit
       :do
         (sdl2:next-event event :poll nil)
         (let* ((event-type (sdl2:get-event-type event)))
           (if (eq :QUIT event-type)
               (setf event-quit t))
         (sdl2:delay 10)))))

(defun main ()
  (sdl2:init :video)
  (let* ((window (sdl2:create-window :title "SDL Surface"
                                     :x sdl2-ffi:+sdl-windowpos-centered+
                                     :y sdl2-ffi:+sdl-windowpos-centered+
                                     :w 800
                                     :h 600
                                     :flags '(:opengl)))
         (renderer (sdl2:create-renderer window nil '(:ACCELERATED))))

    (sdl2:render-clear renderer)
    (sdl2:set-render-draw-color renderer 0 0 #xFF #xFF)
    (sdl2:render-fill-rect renderer (sdl2:make-rect 10 10 780 580))

    (let* ((image (sdl2-image:load-image "resources/images/lisp.png"))
           (width (sdl2:surface-width image))
           (height (sdl2:surface-height image))
           (tex-image (sdl2:create-texture-from-surface renderer image)))
      (sdl2:query-texture tex-image)
      (sdl2:render-copy renderer tex-image
                        :source-rect (sdl2:make-rect 0 0 width height)
                        :dest-rect (sdl2:make-rect 0 0 width height))
      (format t "width = ~A height = ~A~%" width height)
      (let ((tex-image (sdl2:create-texture-from-surface renderer image)))
        (multiple-value-bind (tex-format tex-access tex-width tex-height)
            (sdl2:query-texture tex-image)
          (format t "width = ~A height = ~A~%" tex-width tex-height))
        (sdl2:render-copy renderer tex-image
                          :source-rect (sdl2:make-rect 0 0 width height)
                          :dest-rect (sdl2:make-rect 0 0 width height)))
      (sdl2:free-surface image)
      (sdl2:destroy-texture tex-image))
    (sdl2:render-present renderer)
    (sdl2:destroy-renderer renderer)

    (let ((event nil))
      (unwind-protect (setf sdl2::*event-loop* t)
        (sdl2:in-main-thread
         (:background nil)
         (sdl2:with-sdl-event (event)
           (main-loop event)))
        (setf sdl2::*event-loop* nil)))
    (sdl2:destroy-window window))
  (sdl2:quit))
